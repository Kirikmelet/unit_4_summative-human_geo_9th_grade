extends KinematicBody

# Main movement file for Godot
var velocity: Vector3 = Vector3()
var mouse_delta: Vector2 = Vector2()
const maxup_down = [-90.0, 90.0]

# User values (change if needed)
var movement_speed = 3
var look_sensitivity = 10

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
func _process(delta):
	$Camera.rotation_degrees.x -= mouse_delta.y * look_sensitivity * delta
	$Camera.rotation_degrees.x = clamp(
		$Camera.rotation_degrees.x, maxup_down[0], maxup_down[1])
	rotation_degrees.y -= mouse_delta.x * look_sensitivity * delta
	mouse_delta = Vector2()
	pass

func _input(event):
	if (event is InputEventMouseMotion):
		mouse_delta = event.relative

func _physics_process(delta):
		var input_vel: Vector2 = Vector2()
		velocity.x = 0
		velocity.z = 0
		if (Input.is_action_pressed("move_forward")):
				input_vel.y += 1
				$AnimationPlayer.play("Walking")
		if (Input.is_action_pressed("move_backward")):
				input_vel.y -= 1
				$AnimationPlayer.play("Walking")
		if (Input.is_action_pressed("move_left")):
				input_vel.x += 1
				$AnimationPlayer.play("Walking")
		if (Input.is_action_pressed("move_right")):
				input_vel.x -= 1
				$AnimationPlayer.play("Walking")
		if (Input.is_key_pressed(KEY_Q)):
			get_tree().quit()
		if (input_vel.x == 0 && input_vel.y == 0):
			$AnimationPlayer.stop(true)
		input_vel = input_vel.normalized()
		var forward: Vector3 = global_transform.basis.z
		var sideway: Vector3 = global_transform.basis.x

		var relative_movement: Vector3 = (forward * input_vel.y + sideway * input_vel.x)

		velocity.x = relative_movement.x * movement_speed
		velocity.z = relative_movement.z * movement_speed

		velocity.y -= 12 * delta

		velocity = move_and_slide(velocity, Vector3.UP)

		if (Input.is_action_pressed("move_jump") && is_on_floor()):
		   velocity.y += 3
