extends DialogControl

func _ready():
	await_input()
	start_dialog()

func quit_handle():
	get_tree().quit()
