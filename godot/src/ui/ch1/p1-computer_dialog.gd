extends DialogControl
signal open_door


func quit_handle() -> void:
	emit_signal("open_door")
	pass
