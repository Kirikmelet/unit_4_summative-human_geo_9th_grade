extends Control
class_name DialogControl

# User variables
export var dialog_json: String = ""
export var next_scene: String = ""

# DO NOT TOUCH
var wait_flag: bool = false
var pause_flag: bool = false
var cur_index: int = 0
var dialog_data: Array = []

func return_json(path: String) -> Array:
	var file = File.new()
	file.open(path, File.READ)
	var ret_json = JSON.parse(file.get_as_text())
	file.close()
	return ret_json.result
	
func _quit() ->  void:
	self.visible = false
	wait_flag = false
	quit_handle()

func check_pause() -> bool:
	if pause_flag: return true
	else: return false

func _pause() -> void:
	pause_flag = true
	self.visible = false

func quit_handle() -> void: # You can override this
	get_tree().change_scene(next_scene)

func await_input() -> void:
	dialog_data = return_json(dialog_json)
	wait_flag = true

func start_dialog() -> void:
	if (cur_index >= len(dialog_data)):
		cur_index = 0
		_quit()
		return
	$Panel/Name.text = dialog_data[cur_index]["name"]
	$Panel/Text.text = dialog_data[cur_index]["text"]
	cur_index += 1
	return

func _input(event) -> void:
	if (!wait_flag || pause_flag): return
	if (event is InputEventKey):
			if (event.is_action_pressed("interact_object")):
				self.visible = true
				start_dialog()
