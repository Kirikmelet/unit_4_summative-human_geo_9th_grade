extends Area

func _on_Area_body_entered(body):
	if (body.get_name() == "Main Character"):
		if ($Dialog.check_pause()):
			$Dialog.visible = true
			$Dialog.pause_flag = false
		else: $Dialog.await_input()
	pass # Replace with function body.

func _on_Area_body_exited(body):
	if (body.get_name() == "Main Character"):
		if ($Dialog.cur_index < 1):
			$Dialog.visible = false
			$Dialog.wait_flag = false
		else: $Dialog._pause()
	pass # Replace with function body.
	
