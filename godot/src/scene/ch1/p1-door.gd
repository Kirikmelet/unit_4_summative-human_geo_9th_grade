extends Spatial

var open_door: bool = false
var in_area: bool = false

func _on_Dialog_open_door():
	open_door = true
	pass # Replace with function body.

func _input(event) -> void:
	if (!open_door || !in_area):
		return
	if (event is InputEventKey):
		if (event.is_action_pressed("interact_object")):
			print(open_door)
			print(in_area)
			get_tree().change_scene("res://scenes/ch1/p2.tscn")


func _on_Area_body_entered(body):
	if (body.name == "Main Character"):
		in_area = true
	pass # Replace with function body.

func _on_Area_body_exited(body):
	if (body.name == "Main Character"):
		in_area = false
	pass # Replace with function body.
